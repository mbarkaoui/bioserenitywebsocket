//
//  BioSerenityWebSocketTests.swift
//  BioSerenityWebSocketTests
//
//  Created by Malek BARKAOUI on 02/03/2021.
//

import XCTest
@testable import BioSerenityWebSocket

class CarViewModelTests: XCTestCase {
    func test_WhenCarisMappedToViewModel(){
        let car = Car(brand: "Ferrari", name: "458", speedMax: 230, cv: 240, currentSpeed: 0)
        let viewModel = CarViewModel(car: car)
        XCTAssertEqual(viewModel.brand, car.brand)
        XCTAssertEqual(viewModel.name, car.name)
        XCTAssertEqual(viewModel.speedMax, "\(car.speedMax) km/h")
        XCTAssertEqual(viewModel.cv, "\(car.cv) cv")
        XCTAssertEqual(viewModel.brand, car.brand)
        XCTAssertEqual(viewModel.speedMaxValue, Float(car.speedMax))
        XCTAssertEqual(viewModel.brandLogo, UIImage(named: car.brand))
    }
}
