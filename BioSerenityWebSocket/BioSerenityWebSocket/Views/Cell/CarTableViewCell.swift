//
//  CarTableViewCell.swift
//  BioSerenityWebSocket
//
//  Created by Malek BARKAOUI on 04/03/2021.
//

import UIKit
import WMGaugeView
import ReactiveSwift
import ReactiveCocoa


class CarTableViewCell: UITableViewCell {
    
    var viewModel: CarViewModel?
    
    // MARK:Outlets
    
    
    @IBOutlet weak var cellContainer: UIView!
    // MARK:Labels
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var cv: UILabel!
    @IBOutlet weak var speedMax: UILabel!
    
    // MARK:UIViews
    @IBOutlet weak var carLogoImageView: UIImageView!
    @IBOutlet weak var speedView: WMGaugeView!
    @IBOutlet weak var buttonsStackView: UIStackView!
    @IBOutlet weak var speedLabel: UILabel!
    
    // MARK:UIButtons
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellContainer.addShadow(offset: CGSize.init(width: 0, height: 2), color: UIColor.black, radius:30.0, opacity: 0.2)
    }
    
    
    // MARK:IBActions
    
    @IBAction func startRunning(_ sender: Any) {
        self.viewModel?.start()
    }
    
    @IBAction func stopRuning(_ sender: Any) {
        self.viewModel?.stop()
    }
    
    //MARK:Functions
    func configure(viewModel: CarViewModel){
        viewModel.connectSocket()

        self.viewModel = viewModel
        self.carName.text = viewModel.name
        self.cv.text = String(viewModel.cv)
        self.speedMax.text = String(viewModel.speedMax)
        self.carLogoImageView.image = viewModel.brandLogo
        
        bindUI()
        setupUI()

    }
   
    
    func setupUI(){
        guard let viewModel = self.viewModel else {
            return
        }
        
        buttonsStackView.layer.cornerRadius = 15
        buttonsStackView.layer.maskedCorners = [.layerMinXMaxYCorner , .layerMaxXMaxYCorner]
        buttonsStackView.clipsToBounds = true
        
        speedView.maxValue = viewModel.speedMaxValue
        speedView.scaleDivisions = 1
        speedView.scaleSubdivisions = 10
        speedView.scaleStartAngle = 25
        speedView.scaleEndAngle = 300
        speedView.showScaleShadow = false;
        speedView.scaleFont = UIFont(name: "AvenirNext-Bold", size: 0.085)
        speedView.scaleDivisionColor = .white;
        speedView.scaleSubDivisionColor = .green;
        speedView.scalesubdivisionsaligment = WMGaugeViewSubdivisionsAlignmentCenter
        speedView.scaleSubdivisionsWidth = 0.010
        speedView.scaleSubdivisionsLength = 0.04
        speedView.scaleDivisionsWidth = 0.007
        speedView.scaleDivisionsLength = 0.07
        speedView.backgroundColor = .clear
    }
    
    
    func bindUI(){
        
        guard let viewModel = self.viewModel else {
            return
        }
        
        viewModel.$actualSpeed.producer.startWithValues { carspeed in
            guard let carspeed = carspeed else {
                return
            }
            
            self.speedView.setValue(abs(Float(carspeed)), animated: true, duration: 3)
            self.speedLabel.text = "\(abs(Float(carspeed))) Km/h "
        }
        
        self.startButton.reactive.isEnabled <~ viewModel.$isStarted.map{!$0}
        self.stopButton.reactive.isEnabled <~ viewModel.$isStarted.map{$0}
    }
    
}
