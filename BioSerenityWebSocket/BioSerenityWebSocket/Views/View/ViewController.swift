//
//  ViewController.swift
//  BioSerenityWebSocket
//
//  Created by Malek BARKAOUI on 02/03/2021.
//

import UIKit
import Starscream

class ViewController: UIViewController,Storyboarded {
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:Properties
    var viewModel: ViewModel?
    var carsViewModels: [CarViewModelProtocol] = []
    
    // MARK:Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.setupBinding()
    }
    
    func setupUI(){
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "CarTableViewCell", bundle: nil), forCellReuseIdentifier: "CarTableViewCell")
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
    }
    
    func setupBinding(){
        guard let viewModel = self.viewModel else {
            return
        }
        viewModel.connectSocket()
        viewModel.$cars.producer.startWithValues { carsItems in
            
            guard let carsItems = carsItems else {
                return
            }
            self.carsViewModels = carsItems
            self.tableView.reloadData()
        }
    }
}

extension ViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carsViewModels.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 0, 30, 0)
        cell.layer.transform = rotationTransform
        cell.alpha = 0
        UIView.animate(withDuration: 0.50) {
            cell.layer.transform = CATransform3DIdentity
            cell.alpha = 1.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarTableViewCell", for: indexPath) as! CarTableViewCell
        cell.configure(viewModel: carsViewModels[indexPath.row] as! CarViewModel)
        return cell
    }
    
    
}


