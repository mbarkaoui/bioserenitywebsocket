//
//  UIVieWExtension.swift
//  BioSerenityWebSocket
//
//  Created by Malek BARKAOUI on 05/03/2021.
//

import Foundation
import UIKit

extension UIView {

    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        
        let shadowLayer = CAShapeLayer()
             layer.cornerRadius = radius
        layer.backgroundColor = UIColor.systemGray6.cgColor
//
//        shadowLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height),
//                 cornerRadius: radius).cgPath
//              shadowLayer.fillColor = backgroundColor?.cgColor
//              shadowLayer.shadowColor = color.cgColor
//              shadowLayer.shadowPath = shadowLayer.path
//              shadowLayer.shadowOffset = offset
//              shadowLayer.shadowOpacity = opacity
//              shadowLayer.shadowRadius = radius
//              layer.insertSublayer(shadowLayer, at: 0)

    }
}


protocol Storyboarded {
    static func instantiate(storyboardName: String) -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate(storyboardName: String) -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        return storyboard.instantiateViewController(identifier: id) as! Self
    }
}

