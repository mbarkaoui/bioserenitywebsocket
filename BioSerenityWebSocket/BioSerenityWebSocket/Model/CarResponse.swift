//
//  CarResponse.swift
//  BioSerenityWebSocket
//
//  Created by Malek BARKAOUI on 04/03/2021.
//

import Foundation
struct CarResponse {
    let car: Car
}

extension CarResponse: Decodable {
    
    private enum UserResponseCodingKeys: String, CodingKey {
        case car = "payload"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: UserResponseCodingKeys.self)
        car = try container.decode(Car.self, forKey: .car)
    }
}
