//
//  CarsResponse.swift
//  BioSerenityWebSocket
//
//  Created by Malek BARKAOUI on 04/03/2021.
//

import Foundation
struct CarsResponse {
    let cars: [Car]
}

extension CarsResponse: Decodable {
    
    private enum UserResponseCodingKeys: String, CodingKey {
        case cars = "payload"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: UserResponseCodingKeys.self)
        cars = try container.decode([Car].self, forKey: .cars)
    }
}

