//
//  Car.swift
//  BioSerenityWebSocket
//
//  Created by Malek BARKAOUI on 04/03/2021.
//

import Foundation

struct Car  {

    var brand: String
    var name: String
    var speedMax: Int
    var cv:Int
    var currentSpeed:Float
}

extension Car: Decodable {
    
    enum UserCodingKeys: String, CodingKey {
        case brand
        case name
        case speedMax
        case cv
        case currentSpeed
    }
    
    init(from decoder: Decoder) throws {
        let userContainer = try decoder.container(keyedBy: UserCodingKeys.self)
        brand = try userContainer.decode(String.self, forKey: .brand)
        name = try userContainer.decode(String.self, forKey: .name)
        speedMax = try userContainer.decode(Int.self, forKey: .speedMax)
        cv = try userContainer.decode(Int.self, forKey: .cv)
        currentSpeed = try userContainer.decode(Float.self, forKey: .currentSpeed)

    }
}
