//
//  CarViewModel.swift
//  BioSerenityWebSocket
//
//  Created by Malek BARKAOUI on 04/03/2021.
//

import Foundation
import ReactiveSwift
import Starscream
import UIKit

    //MARK:Protocols

protocol CarViewModelProtocol {
    func start()
    func stop()
}

class CarViewModel: CarViewModelProtocol {
    
    //MARK:MutableProperties
    
    @MutableProperty var isStarted:Bool = false
    @MutableProperty var actualSpeed:Int? = nil
    var socket:WebSocket? = nil
    
    //MARK:Properties
    
    var brand: String = ""
    var name: String
    var speedMax: String
    var speedMaxValue:Float
    var cv:String
    var currentSpeed:Float
    var brandLogo:UIImage
    
    init(car: Car) {
        self.brand = car.brand
        self.name = car.name
        self.speedMax = "\(car.speedMax) km/h"
        self.cv = "\(car.cv) cv"
        self.currentSpeed = car.currentSpeed
        self.speedMaxValue = Float(car.speedMax)
        
        switch self.brand {
        case "Peugeot":
            self.brandLogo = UIImage(named: "Peugeot")!
        case "Porsche":
            self.brandLogo = UIImage(named: "Porsche")!
        case "Jaguar":
            self.brandLogo = UIImage(named: "Jaguar")!
        case "BMW":
            self.brandLogo = UIImage(named: "BMW")!
        case "Audi":
            self.brandLogo = UIImage(named: "Audi")!
        case "Aston Martin":
            self.brandLogo = UIImage(named: "AstonMartin")!
        case "Ferrari":
            self.brandLogo = UIImage(named: "Ferrari")!
            
        default:
            self.brandLogo = UIImage(named: "BMW")!
        }
        
    }
    
    func connectSocket() {
        let request = URLRequest(url: URL(string: "ws://testpostulant.bioserenity.cloud:8080/openSocket")!)
        socket = WebSocket(request: request)
        socket?.delegate = self
        socket!.connect()
    }
    
    
    func start() {
        
        self.$isStarted.modify({$0 = true})
        let dict: [String: Any] =  ["type":"start","userToken":42,"payload":["name":self.name]]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
            self.socket!.write(data: jsonData)
        }
        
        catch {
            print(error.localizedDescription)
        }
    }
    
    
    func stop() {
        self.$isStarted.modify({$0 = false})
        self.$actualSpeed.modify{$0 = Int(0.0)}
        let dict: [String: Any] =  ["type":"stop","userToken":42]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
            self.socket!.write(data: jsonData)
        }
        catch {
            print(error.localizedDescription)
        }
    }
}


extension CarViewModel: WebSocketDelegate {
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            print("connected")
            print("websocket is connected: \(headers)")
        case .disconnected(let reason, let code):
            print("looool")
            print("websocket is disconnected: \(reason) with code: \(code)")
        case .text(let string):
            print("Received text: \(string)")
            
            do {
                guard let CarsList: Data = string.data(using: .utf8) else {
                    return
                }
                
                let apiResponse = try JSONDecoder().decode(CarResponse.self, from: CarsList)
                self.$actualSpeed.modify { $0 = Int(apiResponse.car.currentSpeed) }
                
            } catch {
                print("error decoding")
            }
            
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            self.connectSocket()
            break
        case .cancelled:
            print("looool")
        case .error(let error):
            print(error ?? "")
        }
    }
}
