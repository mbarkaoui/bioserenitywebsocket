//
//  viewModel.swift
//  BioSerenityWebSocket
//
//  Created by Malek BARKAOUI on 03/03/2021.
//

import Foundation
import Starscream
import ReactiveSwift

protocol ViewModelProtocol {
    func connectSocket()
    func getCarsData()
}


class ViewModel:ViewModelProtocol {
    
    @MutableProperty var cars:[CarViewModelProtocol]? = nil
    var socket:WebSocket? = nil
    
    
    func connectSocket() {
        let request = URLRequest(url: URL(string: "ws://testpostulant.bioserenity.cloud:8080/openSocket")!)
        socket = WebSocket(request: request)
        socket?.delegate = self
        socket!.connect()
    }
    
    func getCarsData() {
        // let dict: [String: Any] =  ["type":"start","userToken":42,"payload":["name":"Mini Cooper"]]
        
        let dict: [String: Any] =  ["type":"infos","userToken":42]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
    
            self.socket!.write(data: jsonData)
        }
        
        catch {
            print(error.localizedDescription)
        }
    }
}

extension ViewModel: WebSocketDelegate {
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            print("connected")
            print("websocket is connected: \(headers)")
            self.getCarsData()
        case .disconnected(let reason, let code):
            print("websocket is disconnected: \(reason) with code: \(code)")
        case .text(let string):
            print("Received text: \(string)")
            
            do {
                guard let CarsList: Data = string.data(using: .utf8) else {
                    return
                }
                
                let apiResponse = try JSONDecoder().decode(CarsResponse.self, from: CarsList)
                self.$cars.modify { $0 = apiResponse.cars.map{ return CarViewModel(car: $0) } }
                
            } catch {
                print("Error decoding")
            }
            
            
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            print("Cancelled")
        case .error(let error):
            print(error)
        }
    }
}

